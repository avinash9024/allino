import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import {HttpClientModule} from '@angular/common/http';
import {SharedSectionModule} from './shared-section/shared-section.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {DropdownComponent} from './shared-section/dropdown/dropdown.component';
import {DropdownTriggerForDirective} from './shared-section/dropdown/dropdown-trigger.directive';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    SharedSectionModule,
    BrowserAnimationsModule,
  ],
  providers: [],
  exports: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';
import {AppService} from '../../app.service';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit {

  expanded = true;
  collapsed = false;

  constructor(public appService: AppService) { }

  ngOnInit(): void {
  }
}

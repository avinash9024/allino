import { Component, OnInit } from '@angular/core';
import {AppService} from '../../app.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  constructor(public apiService: AppService) { }

  ngOnInit(): void {
  }

  expandOrCollapse(): void {
    this.apiService.collapsed = !this.apiService.collapsed;
    this.apiService.expanded = !this.apiService.expanded;
  }
}

import {AfterViewInit, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {AppService} from '../../app.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, AfterViewInit {

  bounceLeft = true;
  bounceRight = false;
  signupButton: any;
  loginButton: any;
  userForms: any;

  constructor(public appService: AppService, private cdr: ChangeDetectorRef) {
    this.appService.loginOrSignup = true;
  }

  openSignup(): void{
    this.userForms.classList.remove('bounceRight');
    this.userForms.classList.add('bounceLeft');
  }

  openLogin(): void{
    this.userForms.classList.remove('bounceLeft');
    this.userForms.classList.add('bounceRight');
  }

  ngOnInit(): void {

  }

  ngAfterViewInit(): void {
    this.signupButton = document.getElementById('signup-button');
    this.loginButton = document.getElementById('login-button');
    this.userForms = document.getElementById('user_options-forms');
  /*  this.appService.loginOrSignup = true;
    this.cdr.detectChanges();*/
  }

}

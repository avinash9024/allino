import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModalComponent } from './modal.component';
import {ClickOutsideDirective} from '../directives/click-outside.directive';
import {ModalClickOutsideDirective} from './modalClick.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [ModalComponent, ModalClickOutsideDirective],
  exports: [ModalComponent]
})
export class ModalModule { }

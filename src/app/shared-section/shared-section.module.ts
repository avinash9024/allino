import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import {InlineSVGModule} from 'ng-inline-svg';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { UserInfoComponent } from './user-info/user-info.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import {DropdownComponent} from './dropdown/dropdown.component';
import {DropdownTriggerForDirective} from './dropdown/dropdown-trigger.directive';
import {OverlayModule} from '@angular/cdk/overlay';
import {ModalModule} from './modal/modal.module';
import { ClickOutsideDirective } from './directives/click-outside.directive';
import { TagDirective } from './directives/tag.directive';

@NgModule({
  declarations: [
    NavBarComponent,
    SideBarComponent,
    UserInfoComponent,
    LoginComponent,
    SignupComponent,
    DropdownComponent,
    DropdownTriggerForDirective,
    ClickOutsideDirective,
    TagDirective
  ],
  imports: [
    CommonModule,
    InlineSVGModule,
    OverlayModule,
    ModalModule
  ],
  exports: [
    NavBarComponent,
    SideBarComponent,
    DropdownComponent,
    DropdownTriggerForDirective,
    ClickOutsideDirective,
    TagDirective
  ],
})
export class SharedSectionModule { }

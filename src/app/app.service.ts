import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  expanded = false;
  collapsed = true;
  selectedSection = 0;
  loginOrSignup = false;
  constructor() { }

  returnNameClassBgColor(firstnameElement: string): string {
    if (firstnameElement) {
      return 'bgColor_' + firstnameElement;
    }
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TaskRoutingModule } from './task-routing.module';
import { TaskComponent } from './task.component';
import {InlineSVGModule} from 'ng-inline-svg';
import {AppModule} from '../../app.module';
import {SharedSectionModule} from '../../shared-section/shared-section.module';
import {FormsModule} from '@angular/forms';
import {ModalModule} from '../../shared-section/modal/modal.module';
import { TaskPanelComponent } from './task-panel/task-panel.component';


@NgModule({
  declarations: [TaskComponent, TaskPanelComponent],
  imports: [
    CommonModule,
    TaskRoutingModule,
    InlineSVGModule,
    SharedSectionModule,
    FormsModule,
    ModalModule
  ]
})
export class TaskModule { }

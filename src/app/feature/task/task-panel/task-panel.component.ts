import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-task-panel',
  templateUrl: './task-panel.component.html',
  styleUrls: ['./task-panel.component.css']
})
export class TaskPanelComponent implements OnInit {
  @Input() isLeftOpen: boolean = false;
  @Output() close: EventEmitter<any> = new EventEmitter<any>();
  documentName: string = "Task 1";
  taskDetaislExpandClose:boolean = true;
  constructor(public appService: AppService) { }

  ngOnInit(): void {
  }

  closePanel(): void {
    this.isLeftOpen = false;
    this.close.emit(true);
  }

}

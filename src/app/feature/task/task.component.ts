import { Component, OnInit } from '@angular/core';
import {AppService} from '../../app.service';
import {ModalService} from '../../shared-section/modal/modal.service';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {

  showFavoriteSection: boolean = false;
  showTeamsSection: boolean = false;
  selectedSection:number = 4;
  arr = ['A', 'B', 'C', 'D'];
  favorites: any[] = [];
  bodyText: string;
  openTaskPanel: boolean = false;
  overviewDetaislExpandClose: boolean = true;
  noOfTasks: number[] = [1,1,1,1,1,1,1,1,1,1];
  constructor(public appService: AppService, private modalService: ModalService) { }

  ngOnInit(): void {
    this.appService.selectedSection = 2;
  }

  openModal(id: string): void {
    this.modalService.open(id);
  }

  closeModal(id: string): void {
    this.modalService.close(id);
  }

}

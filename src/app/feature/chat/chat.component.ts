import {Component, ElementRef, OnInit} from '@angular/core';
import {AppService} from '../../app.service';
import {WebSocketServiceService} from './web-socket-service.service';
import {Subject} from 'rxjs';
import * as Rx from 'rxjs';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  rotate = false;
  directShow = false;
  wsConnection: WebSocket;
  callStatus: HTMLElement;
  myConn: webkitRTCPeerConnection;
  yourConnection: RTCPeerConnection;
  dataChannel: RTCDataChannel;
  yourVideo: HTMLMediaElement;
  theirVideo: HTMLMediaElement;
  connectedUser: any;
  stream;

  constructor(public appService: AppService, element: ElementRef) {
    // this.initialiseWebSocket();
    this.yourVideo = element.nativeElement;
    this.theirVideo = element.nativeElement;
  }

  ngOnInit(): void {
  }

  initialiseWebSocket(): void {
    this.wsConnection = new WebSocket('ws://localhost:9092/websocketRTC');
    this.wsConnection.onopen = () => console.log('connected');
    this.wsConnection.onmessage = event => { this.consumeMessage(event); };
    this.wsConnection.onerror = event => { this.logWSError(event); };
  }

  test(): void {
    this.send({
      type: 'login',
      name: 'name'
    });
  }

  call(): void {
    this.startPeerConnection('test');
  }

  startPeerConnection(user): void {
    // this.connectedUser = user;
    //
    // // yourConnection
    // // Begin the offer
    //
    // // send call request 1
    // this.yourConnection.createOffer(offer => {
    //   console.log('    yourConnection.createOffer');
    //   this.send({
    //     type: 'offer',
    //     offer: offer
    //   });
    //
    //   console.log('     yourConnection.setLocalDescription(offer);');
    //   this.yourConnection.setLocalDescription(offer);
    // }, error => {
    //   alert('An error has occurred.');
    // });
  }

  consumeMessage(message: any): void {
    // console.log(message);
    const data = JSON.parse(JSON.stringify(JSON.parse(message.data)));
    console.log(data);
    switch (data.type) {
      case 'login':
        this.onLogin(data.success);
        break;
      case 'offer':
        this.onOffer(data.offer, data.name);
        break;
      case 'answer':
        this.onAnswer(data.answer);
        break;
      case 'candidate':
        this.onCandidate(data.candidate);
        break;
      case 'leave':
        this.onLeave();
        break;
      default:
        console.log('default message');
        console.log(data);
        break;
    }
    console.log(data);
  }

  logWSError(error): void {
    console.log(error);
  }

  onLogin(success): void {
    if (success === false) {
      alert('Login unsuccessful, please try a different name.');
    } else {
      // loginPage.style.display = "none";
      // callPage.style.display = "block";

      // Get the plumbing ready for a call
      // ready to start a connection
      this.startConnection();
    }
  }

  onOffer(offer, name): void {
    // this.connectedUser = name;
    //
    // console.log('============================================================');
    // console.log('===============    onOffer       (===================');
    // console.log('connector user name is'  + this.connectedUser);
    // console.log('============================================================');
    //
    // const offerJson = JSON.parse(JSON.stringify(offer));
    // const sdp = offerJson.sdp;
    //
    // // Set the conversation description of the other party
    // try {
    //   console.log('                   yourConnection.setRemoteDescription                   ');
    //   this.yourConnection.setRemoteDescription(new window.RTCSessionDescription(offerJson));
    // } catch (e) {
    //   alert(e);
    // }
    //
    // // Send a reply message to the call requester 3
    // this.yourConnection.createAnswer(answer => {
    //   this.yourConnection.setLocalDescription(answer);
    //   console.log('               yourConnection.createAnswer                  ');
    //   this.send({
    //     type: 'answer',
    //     answer: answer
    //   });
    // });
    // console.log('onOffer is success');
  }

  onAnswer(answer): void {
    if (this.yourConnection == null) {
      alert('yourconnection is null in onAnswer');
    }

    console.log('============================================================');
    console.log('================ OnAnswer ============================');
    console.log('============================================================');
    console.log(answer);
    if (answer != null) {
      console.log(typeof answer);
    }

    const answerJson = JSON.parse(JSON.stringify(answer));
    console.log(answerJson);

    try {

      // Set the description of this session
      this.yourConnection.setRemoteDescription(new RTCSessionDescription(answerJson));
    } catch (e) {
      alert(e);
    }

    console.log('onAnswer is success');
  }

  onCandidate(candidate): void {
    console.log('============================================================');
    console.log('================ OnCandidate ============================');
    console.log('============================================================');
    console.log(candidate);
    if (candidate != null) {
      console.log(typeof candidate);
    }

    let iceCandidate;

    // try {

    const candidateJson = JSON.parse(JSON.stringify(candidate));
    console.log(candidateJson);

    iceCandidate = new RTCIceCandidate(candidateJson);
    // }catch(e){
    //   console.log("exception is ")
    //   console.log(e);
    // }

    if (this.yourConnection == null) {
      alert('yourconnection is null in onCandidate');
    }
    // yourConnection.addIceCandidate(new RTCIceCandidate(candidate));
    this.yourConnection.addIceCandidate(iceCandidate);
  }

  onLeave(): void {
    this.connectedUser = null;
    this.theirVideo.src = null;
    this.yourConnection.close();
    this.yourConnection.onicecandidate = null;
    this.yourConnection.ontrack = null;
    this.setupPeerConnection(this.stream);
  }

  startConnection(): void {

    // Want to get a camera resolution closest to 1280x720
    const constraints = {audio: true};
    navigator.mediaDevices.getUserMedia(constraints)
      .then(mediaStream => {
        // var video = document.querySelector('video');
        console.log(this.yourVideo);
        this.yourVideo.srcObject = mediaStream;

        if (this.hasRTCPeerConnection()) {
          console.log('setupPeerConnection .. ');
          this.setupPeerConnection(mediaStream);
        } else {
          alert('Sorry, your browser does not support WebRTC.');
        }

        this.yourVideo.onloadedmetadata = e => {
          console.log('your video is playng');
          this.yourVideo.play();
        };


      })
      .catch(err => {
        console.log(err.name + ' -- : ' + err.message);
      });

  }

  hasRTCPeerConnection(): boolean {
    // window.RTCPeerConnection = window.RTCPeerConnection || window.webkitRTCPeerConnection || window.mozRTCPeerConnection;
    window.RTCPeerConnection = window.RTCPeerConnection;
    // window.RTCSessionDescription = window.RTCSessionDescription || window.webkitRTCSessionDescription || window.mozRTCSessionDescription;
    window.RTCSessionDescription = window.RTCSessionDescription ;
    // window.RTCIceCandidate = window.RTCIceCandidate || window.webkitRTCIceCandidate || window.mozRTCIceCandidate;
    window.RTCIceCandidate = window.RTCIceCandidate ;
    return !!window.RTCPeerConnection;
  }

  setupPeerConnection(stream): void {
    if (this.yourConnection == null) {
      const configuration = {
        // "iceServers": [{ "url": "stun:127.0.0.1:9876" }]
        iceServers: [{urls: 'stun:119.3.239.168:3478'},
          {urls: 'turn:119.3.239.168:3478', username: 'codeboy', credential: 'helloworld'}]
      };
      // tslint:disable-next-line:max-line-length
      // this.yourConnection = new RTCPeerConnection(configuration, { optional: [{RtpDataChannels: true}]});  Having issue with second parameter
      this.yourConnection = new RTCPeerConnection(configuration);
    }


    if (this.yourConnection == null) {
      console.log('yourConneion is null');
    } else {
      console.log('yourConnection is a object');
    }

    console.log('========================= setupPeerConnection stream ====================================');
    // console.log(stream);

    // Setup stream listening
    // this.yourConnection.addStream(stream); addStream method is deprecated
    stream.getTracks().forEach(track =>  this.yourConnection.addTrack(track, stream));
    /*  onaddstream method is depricated use ontrack
     this.yourConnection.onaddstream = function (e) {

       console.log(e);
       // theirVideo.src = window.URL.createObjectURL(e.stream);
       theirVideo.srcObject = e.stream;
       theirVideo.play();
     };*/

    this.yourConnection.ontrack = e => {

      console.log(e);
      // theirVideo.src = window.URL.createObjectURL(e.stream);
      this.theirVideo.srcObject = e.streams[0];
      // this.theirVideo.play();
    };

    // Setup ice handling
    this. yourConnection.onicecandidate = event => {
      if (event.candidate) {
        this.send({
          type: 'candidate',
          candidate: event.candidate
        });
      }
    };

    // Open the data channel (this is for text communication)
    this.openDataChannel();
  }

  openDataChannel(): void {
    const dataChannelOptions = {
      ordered: true
    };
    this.dataChannel = this.yourConnection.createDataChannel('myLabel', dataChannelOptions);

    this.dataChannel.onerror = error => {
      console.log('Data Channel Error:', error);
    };

    this.dataChannel.onmessage = event => {
      console.log('Got Data Channel Message:', event.data);

      // received.innerHTML += event.data + '<br />"';
      // received.scrollTop = received.scrollHeight;
    };

    this.dataChannel.onopen = () => {
      this.dataChannel.send(name + 'has connected.');
    };

    this.dataChannel.onclose = () => {
      console.log('The Data Channel is Closed');
    };
  }

  send(message): void {
    if (this.connectedUser) {
      message.name = this.connectedUser;
      message.myName = name;
    }
    console.log(message);
    this.wsConnection.send(JSON.stringify(message));
  }

  /* Udemy Procedure starts */
  loginProcess(success): void {
    if (success === false) {
      alert('Try a different username');
    } else {
      navigator.getUserMedia({
        video: true,
        audio: true
      },  (myStream) => {
        // stream = myStream;
        // local_video.srcObject = stream;

        const configuration = {
          iceServers: [{
            url: 'stun:stun2.1.google.com:19302'
          }]
        }

        /*  this.myConn = new webkitRTCPeerConnection(configuration, {
            optional: [{
              RtpDataChannels: true
            }]
          });*/

        this.dataChannel = this.myConn.createDataChannel('channel1');
        this.dataChannel.onerror = error => {
          console.log('Error :', error);
        };
        // this.dataChannel.onmessage = event => {
        // tslint:disable-next-line:max-line-length
        //   chatArea.innerHTML += "<div class='left-align' style='display:flex;align-items:center;'><img src='assets/images/other.jpg' style='height:40px;width:40px;' class='caller-image circle'><div style='font-weight:600;margin:0 5px;'>" + connected_user + "</div>: <div>" + event.data + "</div></div><br/>";
        // }
        // this.dataChannel.onclose = () => {
        //   console.log('data channel is closed');
        // }
        //
        //
        // this.myConn.addStream(stream);
        // myConn.onaddstream = function (e) {
        //   remote_video.srcObject = e.stream;
        //
        // tslint:disable-next-line:max-line-length
        //   call_status.innerHTML = '<div class="call-status-wrap white-text"> <div class="calling-wrap"> <div class="calling-hang-action"> <div class="videocam-on"> <i class="material-icons teal darken-2 white-text video-toggle">videocam</i> </div> <div class="audio-on"> <i class="material-icons teal darken-2 white-text audio-toggle">mic</i> </div> <div class="call-cancel"> <i class="call-cancel-icon material-icons red darken-3 white-text">call</i> </div> </div> </div> </div>';
        //
        //   const video_toggle = document.querySelector('.videocam-on');
        //   var audio_toggle = document.querySelector('.audio-on');
        //   video_toggle.onclick = function () {
        //     stream.getVideoTracks()[0].enabled = !(stream.getVideoTracks()[0].enabled);
        //
        //     var video_toggle_class = document.querySelector('.video-toggle');
        //     if (video_toggle_class.innerText == 'videocam') {
        //       video_toggle_class.innerText = 'videocam_off';
        //     } else {
        //       video_toggle_class.innerText = 'videocam';
        //     }
        //   }
        //
        //   audio_toggle.onclick = function () {
        //     stream.getAudioTracks()[0].enabled = !(stream.getAudioTracks()[0].enabled);
        //
        //     var audio_toggle_class = document.querySelector('.audio-toggle');
        //     if (video_toggle_class.innerText == 'mic') {
        //       video_toggle_class.innerText = 'mic_off';
        //     } else {
        //       video_toggle_class.innerText = 'mic';
        //     }
        //
        //   }
        //   hangup();
        //
        // }
        //
        // myConn.onicecandidate = function (event) {
        //   if (event.candidate) {
        //     send({
        //       type: "candidate",
        //       candidate: event.candidate
        //
        //     })
        //   }
        // }


      }, error => {
        console.log(error);
      });
    }
  }

  answerProcess(answer): void {
    this.myConn.setRemoteDescription(new RTCSessionDescription(answer));
  }

  candidateProcess(candidate): void {
    this.myConn.addIceCandidate(new RTCIceCandidate(candidate));
  }

  rejectProcess(): void {
    // call_status.innerHTML = '';
  }

  acceptProcess(): void {
    // call_status.innerHTML = '';
    // recordButton.disabled = false;
  }

  leaveProcess(): void {
    // remote_video.src = null;
    this.myConn.close();
    this.myConn.onicecandidate = null;
    // this.myConn.onaddstream = null;
    // connected_user = null;
  }
  /* Udemy Procedure ends */
}

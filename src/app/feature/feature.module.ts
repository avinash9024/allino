import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FeatureRoutingModule } from './feature-routing.module';
import {SharedSectionModule} from '../shared-section/shared-section.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    FeatureRoutingModule,
    SharedSectionModule
  ]
})
export class FeatureModule { }

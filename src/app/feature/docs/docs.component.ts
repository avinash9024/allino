import { Component, OnInit } from '@angular/core';
import {AppService} from '../../app.service';

@Component({
  selector: 'app-docs',
  templateUrl: './docs.component.html',
  styleUrls: ['./docs.component.css']
})
export class DocsComponent implements OnInit {

  showFavoriteSection = false;
  showTeamsSection = false;
  selectedSection = 1;
  arr = ['A', 'B', 'C', 'D'];
  favorites: any[] = [];

  constructor(public appService: AppService) { }

  ngOnInit(): void {
    this.appService.selectedSection = 6;
  }

}
